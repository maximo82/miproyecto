public class Persona implements Interfaz{

    int edad;
    String nombre;
    int id = incremento++;

    static int incremento = 1; //init value

    Persona(int edad, String nombre){
        this.edad = edad;
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public int getEdad(){
        return this.edad;
    }

    public String getNombre() {
        return nombre;
    }

    @Override
    public void comer(){
        System.out.println("Persona comiendo.");
    }

    public void imprimir(){
        System.out.println("id: " + this.getId());
        System.out.println("nombre= "+ this.getNombre());
        System.out.println("edad= " + this.getEdad());
        if(this.id%2==0){      // imprime comer() cuando id es par
            this.comer();
        }
    }
}
