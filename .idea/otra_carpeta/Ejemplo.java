public class Ejemplo{
	public static void main(String [] args){
		
		Persona[] personas = new Persona[3];

		personas[0] = new Persona(24, "Andrea");
		personas[1] = new Persona(85, "Juan");
		personas[2] = new Persona(24, "Pepe");
		personas[3] = new Persona(22, "Camilo");
		
		for (Persona p : personas) {
			System.out.println(System.lineSeparator());
			p.imprimir();
		}
	}
}
